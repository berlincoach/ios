// # Proxy Compiler 19.1.4-aa99e2-20190611

import Foundation
import SAPOData

open class OdataService<Provider: DataServiceProvider>: DataService<Provider> {
    public override init(provider: Provider) {
        super.init(provider: provider)
        self.provider.metadata = OdataServiceMetadata.document
    }

    @available(swift, deprecated: 4.0, renamed: "fetchAddressSet")
    open func addressSet(query: DataQuery = DataQuery()) throws -> [Address] {
        return try self.fetchAddressSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchAddressSet")
    open func addressSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Address]?, Error?) -> Void) {
        self.fetchAddressSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchCustomerSet")
    open func customerSet(query: DataQuery = DataQuery()) throws -> [Customer] {
        return try self.fetchCustomerSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchCustomerSet")
    open func customerSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Customer]?, Error?) -> Void) {
        self.fetchCustomerSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    open func fetchAddress(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Address {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Address>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.addressSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchAddress(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Address?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAddress(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAddressSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Address] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Address.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.addressSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchAddressSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Address]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAddressSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchAddressWithKey(addressID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Address {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchAddress(matching: var_query.withKey(Address.key(addressID: addressID)), headers: headers, options: options)
    }

    open func fetchAddressWithKey(addressID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Address?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchAddressWithKey(addressID: addressID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCustomer(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Customer {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Customer>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.customerSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchCustomer(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Customer?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCustomer(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCustomerSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Customer] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Customer.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.customerSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchCustomerSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Customer]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCustomerSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchCustomerWithKey(customerID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Customer {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchCustomer(matching: var_query.withKey(Customer.key(customerID: customerID)), headers: headers, options: options)
    }

    open func fetchCustomerWithKey(customerID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Customer?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchCustomerWithKey(customerID: customerID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchJob(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Job {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Job>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.jobSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchJob(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Job?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchJob(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchJobSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Job] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Job.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.jobSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchJobSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Job]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchJobSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchJobWithKey(jobID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Job {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchJob(matching: var_query.withKey(Job.key(jobID: jobID)), headers: headers, options: options)
    }

    open func fetchJobWithKey(jobID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Job?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchJobWithKey(jobID: jobID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMachine(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Machine {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Machine>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.machineSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchMachine(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Machine?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMachine(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMachineSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Machine] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Machine.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.machineSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchMachineSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Machine]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMachineSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMachineWithKey(machineID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Machine {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchMachine(matching: var_query.withKey(Machine.key(machineID: machineID)), headers: headers, options: options)
    }

    open func fetchMachineWithKey(machineID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Machine?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMachineWithKey(machineID: machineID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMaterial(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Material {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Material>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.materialSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchMaterial(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Material?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMaterial(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMaterialPosition(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> MaterialPosition {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<MaterialPosition>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.materialPositionSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchMaterialPosition(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (MaterialPosition?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMaterialPosition(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMaterialPositionSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [MaterialPosition] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try MaterialPosition.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.materialPositionSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchMaterialPositionSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([MaterialPosition]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMaterialPositionSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMaterialPositionWithKey(materialPositionID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> MaterialPosition {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchMaterialPosition(matching: var_query.withKey(MaterialPosition.key(materialPositionID: materialPositionID)), headers: headers, options: options)
    }

    open func fetchMaterialPositionWithKey(materialPositionID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (MaterialPosition?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMaterialPositionWithKey(materialPositionID: materialPositionID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMaterialSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Material] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Material.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.materialSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchMaterialSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Material]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMaterialSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchMaterialWithKey(materialID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Material {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchMaterial(matching: var_query.withKey(Material.key(materialID: materialID)), headers: headers, options: options)
    }

    open func fetchMaterialWithKey(materialID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Material?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchMaterialWithKey(materialID: materialID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOrder(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Order {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Order>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.orderSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchOrder(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Order?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOrder(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOrderEvents(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> OrderEvents {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<OrderEvents>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.orderEventsSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchOrderEvents(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (OrderEvents?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOrderEvents(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOrderEventsSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [OrderEvents] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try OrderEvents.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.orderEventsSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchOrderEventsSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([OrderEvents]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOrderEventsSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOrderEventsWithKey(orderEventsID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> OrderEvents {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchOrderEvents(matching: var_query.withKey(OrderEvents.key(orderEventsID: orderEventsID)), headers: headers, options: options)
    }

    open func fetchOrderEventsWithKey(orderEventsID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (OrderEvents?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOrderEventsWithKey(orderEventsID: orderEventsID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOrderSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Order] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Order.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.orderSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchOrderSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Order]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOrderSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchOrderWithKey(orderID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Order {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchOrder(matching: var_query.withKey(Order.key(orderID: orderID)), headers: headers, options: options)
    }

    open func fetchOrderWithKey(orderID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Order?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchOrderWithKey(orderID: orderID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchStep(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Step {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Step>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.stepSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchStep(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Step?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchStep(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchStepSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Step] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Step.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.stepSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchStepSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Step]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchStepSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchStepWithKey(stepID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Step {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchStep(matching: var_query.withKey(Step.key(stepID: stepID)), headers: headers, options: options)
    }

    open func fetchStepWithKey(stepID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Step?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchStepWithKey(stepID: stepID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchTask(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Task {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Task>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.taskSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchTask(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Task?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchTask(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchTaskSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Task] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Task.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.taskSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchTaskSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Task]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchTaskSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchTaskWithKey(taskID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Task {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchTask(matching: var_query.withKey(Task.key(taskID: taskID)), headers: headers, options: options)
    }

    open func fetchTaskWithKey(taskID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Task?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchTaskWithKey(taskID: taskID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchTool(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Tool {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<Tool>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.toolSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchTool(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Tool?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchTool(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchToolPosition(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ToolPosition {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<ToolPosition>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.toolPositionSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchToolPosition(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ToolPosition?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchToolPosition(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchToolPositionSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [ToolPosition] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try ToolPosition.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.toolPositionSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchToolPositionSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([ToolPosition]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchToolPositionSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchToolPositionWithKey(toolPositionID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> ToolPosition {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchToolPosition(matching: var_query.withKey(ToolPosition.key(toolPositionID: toolPositionID)), headers: headers, options: options)
    }

    open func fetchToolPositionWithKey(toolPositionID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (ToolPosition?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchToolPositionWithKey(toolPositionID: toolPositionID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchToolSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [Tool] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try Tool.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.toolSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchToolSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([Tool]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchToolSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchToolWithKey(toolID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> Tool {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchTool(matching: var_query.withKey(Tool.key(toolID: toolID)), headers: headers, options: options)
    }

    open func fetchToolWithKey(toolID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (Tool?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchToolWithKey(toolID: toolID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchUser(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> User {
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try CastRequired<User>.from(self.executeQuery(query.fromDefault(OdataServiceMetadata.EntitySets.userSet), headers: var_headers, options: var_options).requiredEntity())
    }

    open func fetchUser(matching query: DataQuery, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (User?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchUser(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchUserSet(matching query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> [User] {
        let var_query = DataQuery.newIfNull(query: query)
        let var_headers = HTTPHeaders.emptyIfNull(headers: headers)
        let var_options = RequestOptions.noneIfNull(options: options)
        return try User.array(from: self.executeQuery(var_query.fromDefault(OdataServiceMetadata.EntitySets.userSet), headers: var_headers, options: var_options).entityList())
    }

    open func fetchUserSet(matching query: DataQuery = DataQuery(), headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping ([User]?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchUserSet(matching: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchUserWithKey(userID: Int64?, query: DataQuery? = nil, headers: HTTPHeaders? = nil, options: RequestOptions? = nil) throws -> User {
        let var_query = DataQuery.newIfNull(query: query)
        return try self.fetchUser(matching: var_query.withKey(User.key(userID: userID)), headers: headers, options: options)
    }

    open func fetchUserWithKey(userID: Int64?, query: DataQuery?, headers: HTTPHeaders? = nil, options: RequestOptions? = nil, completionHandler: @escaping (User?, Error?) -> Void) {
        self.addBackgroundOperationForFunction {
            do {
                let result = try self.fetchUserWithKey(userID: userID, query: query, headers: headers, options: options)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    @available(swift, deprecated: 4.0, renamed: "fetchJobSet")
    open func jobSet(query: DataQuery = DataQuery()) throws -> [Job] {
        return try self.fetchJobSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchJobSet")
    open func jobSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Job]?, Error?) -> Void) {
        self.fetchJobSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchMachineSet")
    open func machineSet(query: DataQuery = DataQuery()) throws -> [Machine] {
        return try self.fetchMachineSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchMachineSet")
    open func machineSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Machine]?, Error?) -> Void) {
        self.fetchMachineSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchMaterialPositionSet")
    open func materialPositionSet(query: DataQuery = DataQuery()) throws -> [MaterialPosition] {
        return try self.fetchMaterialPositionSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchMaterialPositionSet")
    open func materialPositionSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([MaterialPosition]?, Error?) -> Void) {
        self.fetchMaterialPositionSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchMaterialSet")
    open func materialSet(query: DataQuery = DataQuery()) throws -> [Material] {
        return try self.fetchMaterialSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchMaterialSet")
    open func materialSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Material]?, Error?) -> Void) {
        self.fetchMaterialSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchOrderEventsSet")
    open func orderEventsSet(query: DataQuery = DataQuery()) throws -> [OrderEvents] {
        return try self.fetchOrderEventsSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchOrderEventsSet")
    open func orderEventsSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([OrderEvents]?, Error?) -> Void) {
        self.fetchOrderEventsSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchOrderSet")
    open func orderSet(query: DataQuery = DataQuery()) throws -> [Order] {
        return try self.fetchOrderSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchOrderSet")
    open func orderSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Order]?, Error?) -> Void) {
        self.fetchOrderSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    open override func refreshMetadata() throws {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        do {
            try ProxyInternal.refreshMetadata(service: self, fetcher: nil, options: OdataServiceMetadataParser.options)
            OdataServiceMetadataChanges.merge(metadata: self.metadata)
        }
    }

    @available(swift, deprecated: 4.0, renamed: "fetchStepSet")
    open func stepSet(query: DataQuery = DataQuery()) throws -> [Step] {
        return try self.fetchStepSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchStepSet")
    open func stepSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Step]?, Error?) -> Void) {
        self.fetchStepSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchTaskSet")
    open func taskSet(query: DataQuery = DataQuery()) throws -> [Task] {
        return try self.fetchTaskSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchTaskSet")
    open func taskSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Task]?, Error?) -> Void) {
        self.fetchTaskSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchToolPositionSet")
    open func toolPositionSet(query: DataQuery = DataQuery()) throws -> [ToolPosition] {
        return try self.fetchToolPositionSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchToolPositionSet")
    open func toolPositionSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([ToolPosition]?, Error?) -> Void) {
        self.fetchToolPositionSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchToolSet")
    open func toolSet(query: DataQuery = DataQuery()) throws -> [Tool] {
        return try self.fetchToolSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchToolSet")
    open func toolSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([Tool]?, Error?) -> Void) {
        self.fetchToolSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchUserSet")
    open func userSet(query: DataQuery = DataQuery()) throws -> [User] {
        return try self.fetchUserSet(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchUserSet")
    open func userSet(query: DataQuery = DataQuery(), completionHandler: @escaping ([User]?, Error?) -> Void) {
        self.fetchUserSet(matching: query, headers: nil, options: nil, completionHandler: completionHandler)
    }
}
