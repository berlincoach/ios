//
//  SampleData.swift
//  Design-Controls
//
//  Created by Kuck, Robin on 17.07.19.
//  Copyright © 2019 SAP. All rights reserved.
//

import Foundation

final class SampleData {

    static func getOrders() -> [DemoOrder] {
        return [
            DemoOrder(title: "Defect Coffee Machine", description: "Our lovely used s machine LTX 90 is defect", customer: "IT Company", address: "Munich, Denninger Strasse 26", status: .done),
            DemoOrder(title: "Machine needs Maintenance", description: "Our coffee machine needs Maintenance", customer: "LDB Design", address: "Planegg, Germeringer Strasse 1", status: .inProgress),
            DemoOrder(title: "Coffee Machine Stopped Working", description: "Description", customer: "LDB Design", address: "Planegg, Germeringer Strasse 1", status: .inProgress),
            DemoOrder(title: "Repair Coffee Machine", description: "Description", customer: "WKM Marketing", address: "Garching, Poststrasse 3", status: .open),
            DemoOrder(title: "Maintenance of Coffee Machine", description: "Description", customer: "CC Consulting", address: "Munich, Denninger Strasse 26", status: .done)
        ]
    }
    
    /*
    static func getTasks() -> [Task] {
        return [
            DemoTask(title: "CC Consulting: Repair", address: "Walldorf, Wieslocher Strasse 2", status: .active, date: "01.10.2019"),
            DemoTask(title: "IT Company: Maintenance", address: "Walldorf, Lessingstrasse 5", status: .scheduled, date: "03.10.2019"),
            DemoTask(title: "WKM Marketing: Repair", address: "St. Leon-Rot, Erlengrund 29", status: .open, date: "05.11.2019"),
            DemoTask(title: "CC Consulting: Maintenance", address: "Rauenberg, Eichenweg 10", status: .open, date: "08.11.2019"),
            DemoTask(title: "CC Consulting: Maintenance", address: "Wiesloch, Gartenstraße 66", status: .done, date: "01.07.2019"),
            DemoTask(title: "LDB Design: Repair", address: "Wiesloch, Neues Straessel 13", status: .done, date: "03.07.2019"),
        ]
    }
     */
    
    static func getSteps() -> [String] {
        return [
            "Remove Water Tank and Filter",
            "Insert new Filter",
            "Flush"
        ]
    }
    
    static func getTools() -> [DemoTool] {
        return [
            DemoTool(name: "Container (500ml)"),
            DemoTool(name: "Flat-bladed Screwdriver")
        ]
    }
    
    static func getMaterials() -> [DemoMaterial] {
        return [
            DemoMaterial(name: "Fresh Water (500ml)"),
            DemoMaterial(name: "Filter"),
            DemoMaterial(name: "Seals")
        ]
    }
    
}

struct DemoOrder {
    var title: String
    var description: String
    var customer: String
    var address: String
    var status: OrderStatus
}

struct DemoMaterial {
    var name: String
}

struct DemoTool {
    var name: String
}
