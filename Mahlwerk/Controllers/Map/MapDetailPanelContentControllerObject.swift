//
//  MapDetailPanelContentController.swift
//  Technician-Controls
//
//  Created by Kuck, Robin on 01.08.19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit
import SAPFiori
import MapKit

class MapDetailPanelContentControllerObject: NSObject {
    
    var task: Task
    var coordinate: CLLocationCoordinate2D
    var viewController: MapViewController
    
    init(task: Task, coordinate: CLLocationCoordinate2D, viewController: MapViewController) {
        self.task = task
        self.coordinate = coordinate
        self.viewController = viewController
    }
    
    var headlineText: String? {
        return "\(task.title ?? "")"
    }
    
    var subheadlineText: String? {
        return "Due on \(task.order?.dueDate?.utc().string() ?? "")"
    }
    
}

extension MapDetailPanelContentControllerObject: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: FUIMapDetailPanel.ButtonTableViewCell.reuseIdentifier) as!
                FUIMapDetailPanel.ButtonTableViewCell
            cell.button.titleLabel?.text = "Show Task"
            cell.button.didSelectHandler = { [unowned self] _ in
                self.viewController.mapView.deselectAnnotation(self.viewController.mapView.selectedAnnotations[0],
                                                               animated: true)
                self.viewController.detailPanel.popChildViewController(completion: {
                    self.viewController.presentedViewController?.dismiss(animated: true, completion: {
                        let taskDetailStoryboard = UIStoryboard(name: "TaskDetail", bundle: nil)
                        let taskDetailViewController = taskDetailStoryboard.instantiateInitialViewController() as! TaskDetailViewController
                        taskDetailViewController.task = self.task
                        
                        let mapNavigationViewController = self.viewController.navigationController!
                        mapNavigationViewController.pushViewController(taskDetailViewController, animated: true)
                    })
                })
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: FUIMapDetailPanel.ButtonTableViewCell.reuseIdentifier) as! FUIMapDetailPanel.ButtonTableViewCell
            if let taskStatus = TaskStatus.init(rawValue: self.task.taskStatusID!) {
                switch taskStatus {
                case .open:
                    cell.button.titleLabel?.text = "Schedule"
                case .scheduled:
                    cell.button.titleLabel?.text = "Set Active"
                case .active:
                    cell.button.titleLabel?.text = "Set Done"
                default:
                    break
                }
            }
            cell.button.didSelectHandler = { [unowned self] _ in
                if let taskStatus = TaskStatus.init(rawValue: self.task.taskStatusID!) {
                    // check if there is an active task
                    if taskStatus == TaskStatus.scheduled {
                        for task in self.viewController.tasks {
                            let taskStatus = TaskStatus.init(rawValue: task.taskStatusID!)
                            if taskStatus == .active {
                                let alert = UIAlertController(title: "Error", message: "Please finish your active Task until you start a new one!", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                    self.viewController.detailPanel.presentContainer()
                                }))
                                self.viewController.detailPanel.popChildViewController(completion: {
                                    self.viewController.presentedViewController?.dismiss(animated: true, completion: {
                                        self.viewController.present(alert, animated: true, completion: nil)
                                        return
                                    })
                                })
                            }
                        }
                    }
                    if let targetStatus = TaskStatus.init(rawValue: taskStatus.id+1) {
                        let alert = UIAlertController(title: "Change Status", message: "Change Status of Task to \(targetStatus.text)?", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            switch taskStatus {
                            case TaskStatus.open:
                                self.task.taskStatusID = TaskStatus.scheduled.id
                            case TaskStatus.scheduled:
                                self.task.taskStatusID = TaskStatus.active.id
                            case TaskStatus.active:
                                self.task.taskStatusID = TaskStatus.done.id
                            default:
                                return
                            }
                            //TODO edit open tasks tasks in online store
                            self.viewController.odataService?.updateEntity(self.task, completionHandler: { (error) in
                                if error != nil {
                                } else {
                                    self.viewController.addAnnotations()
                                    self.viewController.odataService?.provider.upload(completionHandler: { (error) in
                                        
                                    })
                                }
                            })
                            self.viewController.detailPanel.presentContainer()
                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                            self.viewController.detailPanel.presentContainer()
                        }))
                        self.viewController.mapView.deselectAnnotation(self.viewController.mapView.selectedAnnotations[0],
                                                                       animated: true)
                        self.viewController.detailPanel.popChildViewController(completion: {
                            self.viewController.presentedViewController?.dismiss(animated: true, completion: {
                                self.viewController.present(alert, animated: true, completion: nil)
                            })
                        })
                    }
                }
            }
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: FUIMapDetailPanel.ButtonTableViewCell.reuseIdentifier) as! FUIMapDetailPanel.ButtonTableViewCell
            cell.button.titleLabel?.text = "Route"
            cell.descriptionLabel.text = "\(task.address?.town ?? ""), \(task.address?.street ?? "") \(task.address?.houseNumber ?? "")"
            cell.button.didSelectHandler = { [unowned self] _ in
                // Display coordinates in navigation app
                let coordinateMake = CLLocationCoordinate2DMake(self.coordinate.latitude, self.coordinate.longitude)
                let region = MKCoordinateRegion(center: coordinateMake, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.02))
                let placemark = MKPlacemark(coordinate: coordinateMake, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)]
                mapItem.name = self.task.title
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Open in Maps", style: .default, handler: { (action) in
                    mapItem.openInMaps(launchOptions: options)
                }))
                alert.addAction(UIAlertAction(title: "Open in Google Maps", style: .default, handler: { (action) in
                    if let url = URL(string: "https://www.google.com/maps/search/?api=1&query=\(self.coordinate.latitude),\(self.coordinate.longitude)") {
                        UIApplication.shared.open(url)
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.definesPresentationContext = true
                alert.show()
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("Clicked")
        case 1:
            print("Clicked")
        default:
            print("Clicked")
        }
    }
    
    
}

extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}


