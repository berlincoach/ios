//
//  OrderHistoryViewController.swift
//  Mahlwerk
//
//  Created by Kuck, Robin on 16.09.19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit
import SAPFiori

class OrderHistoryViewController: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate {

    var historyOrders: [Order] = []
    var filteredOrders = [Order]()
    
    let searchController = FUISearchController(searchResultsController: nil)
    var forceSearchBarVisibleOnLoad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //TODO searchbar causes exception
        //configureSearchbar()
    }
    
    private func configureSearchbar() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.placeholder = "Search Tasks"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        definesPresentationContext = true
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching() {
            return filteredOrders.count
        } else {
            return historyOrders.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        if isSearching() {
            cell.order = filteredOrders[indexPath.row]
        } else {
            cell.order = historyOrders[indexPath.row]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderDetailStoryboard = UIStoryboard(name: "OrderDetail", bundle: nil)
        let orderDetailViewController = orderDetailStoryboard.instantiateInitialViewController() as! OrderDetailViewController
        
        if isSearching() {
            orderDetailViewController.order = filteredOrders[indexPath.row]
        } else {
            orderDetailViewController.order = historyOrders[indexPath.row]
        }
        
        navigationController?.pushViewController(orderDetailViewController, animated: true)
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredOrders = historyOrders.filter({ (order) -> Bool in
            (order.title!.lowercased().contains(searchController.searchBar.text!.lowercased()))
        })
        self.tableView.reloadData()
    }
}
