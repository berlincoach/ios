//
//  OrdersViewController.swift
//  Technician-Controls
//
//  Created by Kuck, Robin on 29.07.19.
//  Copyright © 2019 SAP. All rights reserved.
//

import UIKit
import SAPFiori
import SAPOData
import SAPOfflineOData
import SAPFioriFlows

class OrdersViewController: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate,
SAPFioriLoadingIndicator, Refreshable {
    
    var orders = [Order]()
    var openOrders = [Order]()
    var filteredOrders = [Order]()
    
    let searchController = FUISearchController(searchResultsController: nil)
    
    var loadOrders: ((_ completionHandler: @escaping ([Order]?, Error?) -> Void) -> Void)?
    var odataService : OdataService<OfflineODataProvider>?
    
    var loadingIndicator: FUILoadingIndicatorView? = FUILoadingIndicatorView()
    var myRefreshControl: UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRefreshControl()
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.leftBarButtonItem?.image = FUIIconLibrary.system.me
        
        guard let odataService = OnboardingSessionManager.shared.onboardingSession?.odataController.odataService else {
            AlertHelper.displayAlert(with: "OData service is not reachable, please onboard again.", error: nil,
                                     viewController: self)
            return
        }
        self.odataService = odataService
        
        func fetchOrderSet(_ completionHandler: @escaping ([Order]?, Error?) -> Void) {
            let query = DataQuery().selectAll().expand(Order.orderEvents).expand(Order.task, withQuery:
                DataQuery().expand(Task.machine).expand(Task.job, withQuery:
                    DataQuery().expand(Job.materialPosition, withQuery: DataQuery().expand(MaterialPosition.material))
                        .expand(Job.toolPosition, withQuery: DataQuery().expand(ToolPosition.tool)))).expand(Order.customer, withQuery:
                    DataQuery().expand(Customer.address))
            //TODO use address from order
            //let query = DataQuery().selectAll().expand(Order.address)
            do {
                odataService.fetchOrderSet(matching: query, completionHandler: completionHandler)
            }
        }
        loadOrders = fetchOrderSet
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateTable()
        configureSearchbar()
    }
    
    func handleRefresh(_ sender: Any) {
        self.odataService?.provider.download(completionHandler: { (error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.displayOfflineMessageBar()
                }
            }
            self.loadData {
                DispatchQueue.main.async {
                    self.endRefreshing()
                }
            }
        })
    }
    
    private func loadData(completionHandler: @escaping () -> Void) {
        self.requestOrders { error in
            defer {
                completionHandler()
            }
            if let error = error {
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorLoadingData", value: "Loading data failed!",
                                                                 comment: "XTIT: Title of loading data error pop up."),
                                         error: error, viewController: self)
                return
            }
            DispatchQueue.main.async {
                self.openOrders = self.orders.filter({ (order) -> Bool in
                    let orderStatus = OrderStatus.init(rawValue: order.orderStatusID!)
                    return orderStatus == OrderStatus.open || orderStatus == OrderStatus.inProgress
                })
                self.tableView.reloadData()
            }
        }
    }
    
    private func requestOrders(completionHandler: @escaping (Error?) -> Void) {
        self.loadOrders!() { orders, error in
            if let error = error {
                completionHandler(error)
                return
            }
            self.orders = orders!
            completionHandler(nil)
        }
    }
    
    private func configureSearchbar() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.placeholder = "Search Orders"
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        definesPresentationContext = true
    }
    
    func updateTable() {
        DispatchQueue.global().async {
            self.loadData {
                
            }
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredOrders = orders.filter({ (order) -> Bool in
            let orderStatus = OrderStatus.init(rawValue: order.orderStatusID!)
            return (order.title?.lowercased().contains(searchController.searchBar.text!.lowercased()))! &&
                orderStatus != .done
        })
        self.tableView.reloadData()
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "orderHistorySegue" {
            let viewController = segue.destination as! OrderHistoryViewController
            viewController.historyOrders = orders.filter({ (order) -> Bool in
                let orderStatus = OrderStatus.init(rawValue: order.orderStatusID!)
                return orderStatus == .done
            })
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if isSearching() {
            return 1
        }
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching() {
            return filteredOrders.count
        }
        switch section {
        case 0:
            return openOrders.count
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        if indexPath.section == 0 {
            cell.order = isSearching() ? filteredOrders[indexPath.row] : openOrders[indexPath.row]
        } else {
            return tableView.dequeueReusableCell(withIdentifier: "HistoryCell")!
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let orderDetailStoryboard = UIStoryboard(name: "OrderDetail", bundle: nil)
            let orderDetailViewController = orderDetailStoryboard.instantiateInitialViewController() as! OrderDetailViewController
            
            if isSearching() {
                orderDetailViewController.order = filteredOrders[indexPath.row]
            } else {
                orderDetailViewController.order = openOrders[indexPath.row]
            }
            
            navigationController?.pushViewController(orderDetailViewController, animated: true)
        }
    }
    
}
