//
//  TaskDetailViewController.swift
//  Design-Controls
//
//  Created by Kuck, Robin on 17.07.19.
//  Copyright © 2019 SAP. All rights reserve
//

import UIKit
import SAPFiori
import SAPOData
import SAPOfflineOData
import SAPFioriFlows
import SAPFoundation

enum TaskDetailSegmentedControlState {
    case jobs
    case information
    case materials
}

class TaskDetailViewController: UITableViewController, Refreshable, NavigationBarSegmentedControl, SAPFioriLoadingIndicator {
    
    var segmentedControlState = TaskDetailSegmentedControlState.jobs
    private var _task: Task?
    var task: Task {
        get {
            if _task == nil {
                return Task(withDefaults: true)
            }
            return _task!
        }
        set {
            _task = newValue
        }
    }
    var tasks = [Task]()
    var loadTask: ((_ completionHandler: @escaping (Error?) -> Void) -> Void)?
    
    var jobs = [Job]()
    var suggestedJobs = [Job]()
    
    private var sortedMaterials: [Int64?: [MaterialPosition]] = [:]
    private var sortedTools: [Int64?: [ToolPosition]] = [:]
    
    var materials: [MaterialPosition] = [] {
        didSet(newValue) {
            sortedMaterials = newValue.group(by: \.materialID)
        }
    }
    var tools: [ToolPosition] = [] {
        didSet(newValue) {
            sortedTools = newValue.group(by: \.toolID)
        }
    }
    
    var odataService: OdataService<OfflineODataProvider>?
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    var header: FUIObjectHeader?
    var myRefreshControl: UIRefreshControl = UIRefreshControl()
    var loadingIndicator: FUILoadingIndicatorView? = FUILoadingIndicatorView()
    var segmentedControl: UISegmentedControl = UISegmentedControl(items: ["Jobs", "Information", "Materials"])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRefreshControl(tintColor: UIColor.white)
        
        self.tableView.register(FUITableViewHeaderFooterView.self,
                                forHeaderFooterViewReuseIdentifier: FUITableViewHeaderFooterView.reuseIdentifier)
        self.tableView.register(FUINoteFormCell.self, forCellReuseIdentifier: FUINoteFormCell.reuseIdentifier)
        
        activityIndicator.center = self.view.center
        view.addSubview(activityIndicator)
        
        configureNavigationBar()
        createObjectHeader()
        
        guard let odataService = OnboardingSessionManager.shared.onboardingSession?.odataController.odataService else {
            AlertHelper.displayAlert(with: "OData service is not reachable, please onboard again.", error: nil, viewController: self)
            return
        }
        self.odataService = odataService
        func fetchTask(_ completionHandler: @escaping (Error?) -> Void) {
            do {
                odataService.loadProperty(Task.machine, into: task, completionHandler: completionHandler)
            }
        } 
        loadTask = fetchTask
        
        if let order = task.order {
            do {
                try self.odataService?.loadProperty(Order.task, into: order, query: DataQuery().expand(Task.machine)
                    .expand(Task.job, withQuery: DataQuery().expand(Job.materialPosition, withQuery:
                        DataQuery().expand(MaterialPosition.material, withQuery:
                            DataQuery().expand(Material.materialPosition)))))
                loadData {
                    
                }
            } catch { }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData {
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let frontWindow = UIApplication.shared.keyWindow {
            self.loadingIndicator!.center = frontWindow.center
            frontWindow.addSubview(self.loadingIndicator!)
            self.loadingIndicator!.startAnimating()
            self.loadingIndicator?.dismiss()
        }
    }
    
    private func configureNavigationBar() {
        if let navbar = self.navigationController?.navigationBar {
            navbar.setValue(true, forKey: "hidesShadow")
        }
        
        if let taskStatus = TaskStatus.init(rawValue: task.taskStatusID!) {
            if taskStatus != .done {
                let changeStatusButton = UIBarButtonItem(image: FUIIconLibrary.app.changeStatus, style: .plain,
                                                         target: self,
                                                         action: #selector(changeStatusButtonClicked(sender:)))
                self.navigationItem.rightBarButtonItem = changeStatusButton
            } else {
                self.navigationItem.rightBarButtonItem = nil
            }
        }
        
        addSegmentedControlToNavigationBar()
    }
    
    private func createObjectHeader() {
        header = FUIObjectHeader(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 200))
        if let header = header {
            header.headlineLabel.text = "\(task.title ?? "taskTitle")"
            header.subheadlineText = "Machine: \(task.machine?.name ?? "machineName")"
            let taskStatus = TaskStatus.init(rawValue: task.taskStatusID ?? 0)
            header.tags = [FUITag(title: "Status: \(taskStatus?.text ?? "taskStatus")")]
            var predictedHours = 0
            for job in task.job {
                predictedHours += job.predictedWorkHours ?? 0
            }
            header.footnoteText = "Predicted Time: \(predictedHours) hrs"
            self.tableView.tableHeaderView = header
        }
    }
    
    func handleRefresh(_ sender: Any) {
        self.odataService?.provider.download(completionHandler: { (error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.displayOfflineMessageBar()
                }
            }
            self.loadData {
                DispatchQueue.main.async {
                    self.endRefreshing()
                }
            }
        })
    }
    
    func handleSegmentChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            segmentedControlState = .jobs
        } else if sender.selectedSegmentIndex == 1 {
            segmentedControlState = .information
        } else {
            segmentedControlState = .materials
        }
        self.tableView.reloadData()
    }
    
    func updateTable() {
        DispatchQueue.global().async {
            self.loadData {
                
            }
        }
    }
    
    private func loadData(completionHandler: @escaping () -> Void) {
        self.requestTask { error in
            defer {
                completionHandler()
            }
            if let error = error {
                AlertHelper.displayAlert(with: NSLocalizedString("keyErrorLoadingData", value: "Loading data failed!", comment: "XTIT: Title of loading data error pop up."), error: error, viewController: self)
                return
            }
            DispatchQueue.main.async {
                self.jobs = self.task.job.filter({ (job) -> Bool in
                    !(job.suggested ?? false)
                }).sorted(by: { (a, b) -> Bool in
                    a.jobStatusID! < b.jobStatusID!
                })
                self.suggestedJobs = self.task.job.filter({ (job) -> Bool in
                    job.suggested ?? false
                })
                self.tools = self.jobs.flatMap({$0.toolPosition})
                self.materials = self.jobs.flatMap({$0.materialPosition})
                
                self.configureNavigationBar()
                self.createObjectHeader()
                self.tableView.reloadData()
            }
        }
    }
    
    private func requestTask(completionHandler: @escaping (Error?) -> Void) {
        self.loadTask!() { error in
            if let error = error {
                completionHandler(error)
                return
            }
            completionHandler(nil)
        }
    }
    
    @objc func changeStatusButtonClicked(sender: UIButton) {
        if let taskStatus = TaskStatus.init(rawValue: self.task.taskStatusID!) {
            // check if there is an active task
            if taskStatus == TaskStatus.scheduled {
                for task in self.tasks {
                    let taskStatus = TaskStatus.init(rawValue: task.taskStatusID!)
                    if taskStatus == .active {
                        let alert = UIAlertController(title: "Error", message: "Please finish your active Task until you start a new one!", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return
                    }
                }
            }
            if let targetStatus = TaskStatus.init(rawValue: taskStatus.id+1) {
                // check if there is an active task
                let alert = UIAlertController(title: "Change Status", message: "Change Status of Task to \(targetStatus.text)?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    switch taskStatus {
                    case .done:
                        return
                    default:
                        self.task.taskStatusID = targetStatus.id
                    }
                    
                    if taskStatus == .open {
                        if ConnectivityUtils.isConnected() {
                            self.loadingIndicator?.show(animated: true)
                            self.odataService?.updateEntity(self.task, completionHandler: { (error) in
                                if let error = error {
                                    self.displayMessageBar(text: error.localizedDescription)
                                    return
                                }
                                self.odataService?.provider.upload(completionHandler: { (error) in
                                    DispatchQueue.main.async {
                                        self.loadingIndicator?.dismiss()
                                    }
                                    if let error = error {
                                        self.displayMessageBar(text: error.message!)
                                        // navigate back if scheduling open task failed
                                        self.navigationController?.popViewController(animated: true)
                                        return
                                    }
                                    DispatchQueue.main.async {
                                        self.updateUI()
                                    }
                                })
                            })
                        } else {
                            self.task.taskStatusID = TaskStatus.open.id
                            self.displayOfflineMessageBar()
                            return
                        }
                    } else {
                        self.odataService?.updateEntity(self.task, completionHandler: { (error) in
                            if error == nil {
                                if ConnectivityUtils.isConnected() {
                                    self.odataService?.provider.upload(completionHandler: { (error) in })
                                }
                            }
                        })
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func updateUI() {
        if let taskStatus = TaskStatus.init(rawValue: task.taskStatusID!) {
            self.header?.tags = [FUITag(title: "Status: \(taskStatus.text)")]
            if taskStatus == .done {
                self.navigationItem.rightBarButtonItem = nil
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        switch segmentedControlState {
        case .jobs:
            return 2
        case .information:
            return 3
        case .materials:
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
            FUITableViewHeaderFooterView.reuseIdentifier) as! FUITableViewHeaderFooterView
        view.style = .title
        view.selectionStyle = .none
        
        switch segmentedControlState {
        case .jobs:
            switch section {
            case 0:
                view.titleLabel.text = "Jobs"
            case 1:
                view.titleLabel.text = "Suggested Jobs"
            default:
                return nil
            }
            return view
        case .information:
            switch section {
            case 0:
                view.titleLabel.text = "Location"
            case 1:
                view.titleLabel.text = "Notes"
            default:
                return nil
            }
            return view
        case .materials:
            switch section {
            case 0:
                view.titleLabel.text = "Tools"
            case 1:
                view.titleLabel.text = "Materials"
            default:
                return nil
            }
            return view
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch segmentedControlState {
        case .jobs:
            return 35
        case .information:
            if section == 0 || section == 1 {
                return 35
            } else {
                return 0.1
            }
        case .materials:
            return 35
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentedControlState {
        case .jobs:
            if section == 0 {
                return jobs.count
            } else {
                return suggestedJobs.count
            }
        case .information:
            if section == 0 || section == 1 {
                return 1
            } else {
                // check if there are active steps
                var activeJobs = false
                for job in task.job {
                    if let jobStatus = JobStatus.init(rawValue: job.jobStatusID!) {
                        if jobStatus != .done && !job.suggested! {
                            activeJobs = true
                        }
                    }
                }
                return activeJobs ? 2 : 3
            }
        case .materials:
            if section == 0 {
                return sortedTools.count
            } else {
                return sortedMaterials.count
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segmentedControlState == .information && indexPath.section == 0 {
            return 85
        }
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        switch segmentedControlState {
        case .jobs:
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobCell
            if section == 0 {
                let job = jobs[indexPath.row]
                cell.job = job
                return cell
            } else {
                let job = suggestedJobs[indexPath.row]
                cell.job = job
                return cell
            }
        case .information:
            if indexPath.section == 0 {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "LocationCell") as! LocationCell
                cell.address = task.address
                return cell
            } else if indexPath.section == 1 {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: FUINoteFormCell.reuseIdentifier)
                    as! FUINoteFormCell
                cell.valueTextView.text = "\(task.notes ?? "notes")"
                return cell
            } else {
                if indexPath.row == 0 {
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "CustomerCell")!
                    return cell
                } else if indexPath.row == 1 {
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "OrderCell")!
                    return cell
                } else {
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "ReportCell")!
                    return cell
                }
            }
        case .materials:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ToolOrMaterialCell") as! ToolOrMaterialCell
            if indexPath.section == 0 {
                let toolGroup = Array(sortedTools)[indexPath.row].value
                cell.name = toolGroup[0].tool?.name
            } else {
                let materialGroup = Array(sortedMaterials)[indexPath.row].value
                cell.name = materialGroup[0].material?.name
                cell.amount = "\(materialGroup.map({$0.quantity!}).sum())"
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.tableView.deselectRow(at: indexPath, animated: true)
        switch segmentedControlState {
        case .jobs:
            let jobStoryBoard = UIStoryboard(name: "Job", bundle: nil)
            let jobDetailViewController = jobStoryBoard.instantiateInitialViewController() as! JobDetailViewController
            let job = self.jobs[indexPath.row]
            self.odataService?.loadProperty(Job.step, into: job, completionHandler: { (error) in
                if error == nil {
                    jobDetailViewController.job = job
                    if let taskStatus = TaskStatus.init(rawValue: self.task.taskStatusID!) {
                        jobDetailViewController.taskStatus = taskStatus
                    }
                    self.navigationController?.pushViewController(jobDetailViewController, animated: true)
                }
            })
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if let taskStatus = TaskStatus.init(rawValue: task.taskStatusID!) {
            if taskStatus == .active || taskStatus == .scheduled {
                switch segmentedControlState {
                case .jobs:
                    if indexPath.section == 0 {
                        if let jobStatus = JobStatus.init(rawValue: jobs[indexPath.row].jobStatusID!) {
                            switch jobStatus {
                            case .open:
                                let doneAction = UITableViewRowAction(style: .default, title: "Done") { (action, indexPath) in
                                    self.changeJobStatus(job: self.jobs[indexPath.row])
                                }
                                doneAction.backgroundColor = JobStatus.done.color
                                return [doneAction]
                            default:
                                break
                            }
                        }
                    } else {
                        let addAction = UITableViewRowAction(style: .default, title: "Add") { (action, indexPath) in
                            self.changeJobStatus(job: self.suggestedJobs[indexPath.row])
                        }
                        addAction.backgroundColor = UIColor.preferredFioriColor(forStyle: FUIColorStyle.positive)
                        return [addAction]
                    }
                default:
                    break
                }
            }
        }
        return []
    }
    
    private func changeJobStatus(job: Job) {
        if job.suggested! {
            job.suggested = false
            job.jobStatusID = JobStatus.open.id
        } else {
            job.jobStatusID = JobStatus.done.id
        }
        self.odataService?.updateEntity(job, completionHandler: { (error) in
            if error != nil {
                self.displayMessageBar(text: "Unable to add suggested Job.")
                return
            }
            self.loadData { }
            self.odataService?.provider.upload(completionHandler: { (error) in })
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "customerSegue" {
            let customerViewController = segue.destination as! CustomerViewController
            customerViewController.customer = task.order!.customer!
        } else if segue.identifier == "finalReportSegue" {
            let finalReportViewController = segue.destination as! FinalReportViewController
            if let order = task.order {
                finalReportViewController.order = order
            }
        } else if segue.identifier == "orderDetailSegue" {
            if let orderDetailViewController = segue.destination as? OrderDetailViewController {
                if let order = task.order {
                    orderDetailViewController.order = order
                    //TODO expand order
                }
            }
        }
    }
}

